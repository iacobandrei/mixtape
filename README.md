# Mixtape

## Building and running

### Requirements

[NodeJS](https://nodejs.org/en/download/package-manager/)

### Building

`npm install && npm test`

### Installing (optional)

`npm install -g`

### Running

If installed, run the `mixtape` command.

To run without installing, run `./bin/index.js` from the package folder.

```
 ./bin/index.js --help
Usage: index.js --in [path_to_input] --commands [path_to_commands_file] --out
[path_to_output]

Options:
  --help      Show help                                                [boolean]
  --version   Show version number                                      [boolean]
  --in        JSON file containing the mixtape                        [required]
  --commands  JSON file containing a set of operations                [required]
  --out       Where to write the updated mixtape                      [required]
```

### Running the example

`./bin/index.js --in data/mixtape.json --commands data/example-ops.json --out out.json`

## Notes about scalability

We need to:

a. handle large command files

b. handle large amounts of mixtape data

c. the data needs to be consistent

d. a. and b. need to be fast

### Input files
- for a large commands file, a CSV format would save some storage space at the expense of the lack of proper standardization/validation but considering that a compressed version of JSON would offer an acceptable size along with machine and human readability, I don't believe it's a fair trade-off and I would advocate for keeping JSON. JSON and YAML are equivalent in this regard, as the former is a subset of the latter, and I don't see a clear scenario where the specific capabilities of YAML (comments, self-reference, etc..) would offer any advantage.
- for both input files, a different parsing technique is warranted as opposed to the one I used in this project. It is both time consuming and limiting by the main memory of the machine to read the whole files and create an in-memory data structure. To make this handle large files, I would use an event-based/streaming parser which would emit an event for each object when it is read from the JSON file. These events can be fed into a separate [reactor](https://en.wikipedia.org/wiki/Reactor_pattern) or could be queued idependently based on event type.
- like component which would process them as needed. Schema validation can still be performed on a per-object basis as opposed to a full document.

### Storage
- for this particular problem, I don't believe a document database offers any advantage over a traditional relational database unless the scenario scales out to the point of not fitting into a single cluster. Even then, the data can be partitioned based on domains (users get abstracted behind a user management service with it's own storage and so on for songs and playlists). The main reason for this is that although a relational database locks and risks becoming a performance bottleneck, considering the manner in which the system is used (batch processing), the QPS does not seem to be the main concern (I am not aware of the SLA). The advantage here is the out-of-the box consistency of object identities.
- if we reach the point of spreading the same data type across different systems, (for example the number of songs is so large that we need to shard it across machines) I'm not sure about the durability requirements but if they are weak, a reactor-like in-memory solution (such as Redis) would offer great performance for the object id problem (could act as a fast coordinator between components) and so would any consensus based solutions (such as Zookeeper).

### Processing
- if the data model is designed as a [CRDT](https://en.wikipedia.org/wiki/Conflict-free_replicated_data_type), we can process commands/updates in parallel in any particular order as the data will eventually become consistent (when the batch process is done). This would offer a great performance increase, as there would be no per-request validation. For example, the system can process remove-playlist before add-playlist and add-songs by using tombstones. Also, add-songs can be processed before add-playlist by adding shallow objects which would later be replaced by add-playlist (or purged in the end). This way, the system can be decoupled (reading commands, executing commands and updating data) and scaled independently. We would be applying a global convergence algorithm to deal with all partial objects after we're done processing the batch commands (for example hard deleting all lists).
- any validation for the existence of the list or song could be done fast by using caches. However, as the data grows a set of caches ends up having the same consistency problems as the data itself so a CRDT model would avoid all of that.
- another option, as I mentioned queues in the first section is to process the queue events async but sequentually by type. For example, create all lists first, then add the songs then delete all lists. So within each queue, the order does not matter so we can scale the 'workers' (entities which actually execute the operations) horizontally.

## Notes

- I initially wanted to do it using Ruby, however I ended up picking JavaScript for the native JSON capabilities and the ease of using functional programming
- schemas were built using https://jsonschema.net/