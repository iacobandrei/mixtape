/* jshint node: true, undef: true, unused: true, esversion: 6 */
/* global describe, it, require */
'use strict';

const expect = require('chai').expect;
const mixtape = require('../lib/mixtape');
const tests = require('./test-cases.json');

describe('end-to-end-tests', () => {
  for (const testCase of tests) {
    it(testCase.name, () => {
      expect(
          mixtape(testCase.data, testCase.ops))
          .to
          .deep
          .equal(testCase.output);
    });
  }
});