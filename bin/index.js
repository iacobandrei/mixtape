#!/usr/bin/env node
/* global console, require */

'use strict';

const mixtape = require('../lib/mixtape');
const fs = require('fs');

const argv = require('yargs')
    .usage('Usage: $0 --in [path_to_input] --commands [path_to_commands_file] --out [path_to_output]')
    .demandOption(['in', 'out', 'commands'])
    .describe('in', 'JSON file containing the mixtape')
    .describe('commands', 'JSON file containing a set of operations')
    .describe('out', 'Where to write the updated mixtape')
    .argv;

fs.readFile(argv.in, (err, mixtapeContent) => {
  if (err) {
    console.error('Could not find input file');
    throw err;
  }

  fs.readFile(argv.commands, (err, commandsContent) => {
    if (err) {
      console.error('Could not find commands file');
      throw err;
    }

    const result = mixtape(
        JSON.parse(mixtapeContent),
        JSON.parse(commandsContent));

    if (result.opResult.length > 0) {
      console.error(JSON.stringify(result.opResult));
    }

    fs.writeFile(
        argv.out,
        JSON.stringify(result.data, null, 2),
        (err) => {
          if (err) {
            console.error('Could not write output');
            throw err;
          }
        });
  });
});