/* jshint node: true, undef: true, unused: true, esversion: 6 */
/* global module, require */
'use strict';

const mixtapeSchema = require('../schema/mixtape');
const operationsSchema = require('../schema/operations');
const operation = require('./operation');
const error = require('./operation').error;
const Ajv = require('ajv');

const avj = new Ajv({allErrors: true});
const validOperationsSchema = avj.compile(operationsSchema);
const validMixtapeSchema = avj.compile(mixtapeSchema);

const cachedCollections = ['playlists', 'users', 'songs'];
const metadata = ['cache', 'playlistTombstones'];

const addMetadata = (data) =>
  Object.assign(
      data,
      {
        'cache':
        // for each element type which needs to be cached, create a property
        // which maps to a set containing all ids of objects of the said type
        // Would result in an object like
        //  {
        //    'playlists': [...],
        //    'users': [...],
        //    (...)
        // }
        cachedCollections.reduce(
            (acc, elementType) =>
              Object.assign(
                  acc,
                  {
                    [elementType]:
                    // extracting ids of the specified type of element
                    new Set(data[elementType].map((element) => element.id)),
                  }),
            {}),
        // for eventual consistency, playlists are first soft-deleted
        'playlistTombstones': new Set(),
      });

const stripMetadata = (state) => {
  metadata.forEach((item) => {
    delete state.data[item];
  });

  return state;
};

// Hard delete playlists
const persist = (state) => ({
  ...state,
  'data': {
    ...state.data,
    'playlists': state.data.playlists.filter((playlist) => !state.data.playlistTombstones.has(playlist.id)),
  },
});

module.exports = (data, ops) => {
  const state = {
    'data': addMetadata(data),
    'opResult': [],
  };

  if (!validMixtapeSchema(data)) {
    state.opResult.push(error(`Mixtape not valid ${JSON.stringify(validMixtapeSchema.errors)}`));
    return state;
  }

  if (!validOperationsSchema(ops)) {
    state.opResult.push(error(`Batch list not valid ${JSON.stringify(validOperationsSchema.errors)}`));
    return state;
  }

  // each operation mutates the 'data' object and in case of errors
  // will also emit a result object with the same id
  return stripMetadata(
      persist(
          ops.reduce((accumulator, op) => operation(accumulator, op), state)));
};