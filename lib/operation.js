/* jshint node: true, undef: true, unused: true, esversion: 6 */
/* global module */
'use strict';

const error = (message, opId) => ({
  'id': opId || 'global',
  'message': message,
});

const operations = {
  'removePlaylist': (state, operation) => {
    if (!state.data.cache.playlists.has(operation.removedPlaylistId)) {
      state.opResult.push(error(`Playlist ${operation.removedPlaylistId} does not exist`, operation.id));
      return state;
    }

    state.data.playlistTombstones.add(operation.removedPlaylistId);
    return state;
  },

  'addSongToPlaylist': (state, operation) => {
    if (!state.data.cache.playlists.has(operation.playlistId)) {
      state.opResult.push(error(`Playlist ${operation.playlistId} does not exist`, operation.id));
      return state;
    }

    if (!state.data.cache.songs.has(operation.songId)) {
      state.opResult.push(error(`Song ${operation.songId} does not exist`, operation.id));
      return state;
    }

    state.data.playlists
        .find((playlist) => playlist.id == operation.playlistId)
        .song_ids.push(operation.songId);
    return state;
  },

  'addPlaylist': (state, operation) => {
    if (state.data.cache.playlists.has(operation.newPlaylistId)) {
      state.opResult.push(error(`Playlist ${operation.newPlaylistId} already exists`, operation.id));
      return state;
    }

    const inexistentSongs = operation.songIds.filter((songId) => !state.data.cache.songs.has(songId));
    if (inexistentSongs.length > 0) {
      state.opResult.push(error(`Songs [${inexistentSongs.join(',')}] do not exist`, operation.id));
      return state;
    }

    state.data.cache.playlists.add(operation.newPlaylistId);
    state.data.playlists.push({
      'id': operation.newPlaylistId,
      'user_id': operation.userId,
      'song_ids': operation.songIds,
    });
    return state;
  },
};

module.exports = (state, operation) => operations[operation.op](state, operation);
module.exports.error = error;